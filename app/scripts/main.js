;(function (wnd, $) {

    "use strict";

    var EmbaWood = function () {
        this.Init();
    };

    EmbaWood.prototype = {

        constructor: EmbaWood,

        Init: function () {

            this.fwContainer = '#fwbslider';
            this.fwDelay = 5000;
            this.fwFadeSpeed = 2000;

            this.partnersTable = null;

            this.newsCarousel = $("#owl-news-carousel");
            this.menuButton = $(".menu-logo .toggle-menu-button");

            this.headerContainer = $(".menu-logo");
            this.footerContainer = $("footer");
            this.newsContainer = $(".news-list");
            this.menuContainer = $("nav.main-menu");

            this.showSearchButton = $("footer .meta a.search");
            this.searchForm = $("footer .meta form.search");
            this.pageContentWrap = $(".main-page-content .in");

            this.mainMenuFirstLevel = $(".main-menu>ul>li");

            this.sliderItemsList = [];
            this.sliderActiveSlideIndex = null;

            this.initEvents();

        },

        setNewsPosition: function () {
            if (this.headerContainer.length > 0 && this.footerContainer.length > 0 && this.newsContainer.length > 0) {
                var offsetTop = $(wnd).height() - (this.headerContainer.height() - this.footerContainer.height() - this.newsContainer.height()),
                    topPosition = (offsetTop / 2) - (this.newsContainer.height()),

                    offsetTop2 = $(wnd).height() - (this.footerContainer.height() + this.newsContainer.height()),
                    allHeight = this.headerContainer.height() + this.footerContainer.height() + this.newsContainer.find("ul").height() + 100 + this.menuContainer.height(),
                    advHeight = (allHeight >= $(wnd).height()) ? offsetTop2 + 1000 : offsetTop2;
                this.newsContainer.css({marginTop: (this.isMenuVisible()) ? advHeight : topPosition});
            }
        },

        isMenuVisible: function () {
            return (this.menuButton.hasClass("active"));
        },

        setMinPageHeight: function () {
            if (this.pageContentWrap.length > 0) {
                var minHeightVal = $(window).height() - this.footerContainer.height();
                this.pageContentWrap.css({minHeight: minHeightVal});
            }
        },

        showMenu: function () {
            var offsetTop = $(wnd).height() - (this.footerContainer.height() + this.newsContainer.height()),
                menuHeight = offsetTop - this.headerContainer.height(),
                allHeight = this.headerContainer.height() + this.footerContainer.height() + this.newsContainer.find("ul").height() + 100 + this.menuContainer.height(),
                advHeight = (allHeight >= $(wnd).height()) ? offsetTop + 1000 : offsetTop,
                menuContainerHeight = (allHeight >= $(wnd).height()) ? menuHeight + this.newsContainer.height() : menuHeight,
                pagesMenuContainerHeight = (this.menuContainer.hasClass("single-page-menu") && $(wnd).width() < 768) ? menuContainerHeight + this.newsContainer.height() : menuContainerHeight;

            this.newsContainer.css({marginTop: advHeight});
            this.menuContainer.height(pagesMenuContainerHeight).fadeIn();
            this.menuButton.addClass("active");
        },

        hideMenu: function () {
            this.menuButton.removeClass("active");
            this.menuContainer.fadeOut();
            this.setNewsPosition();
        },

        filterGlobal: function () {
            $('#partners-table').DataTable().search(
                $('#partner-country').find("option:selected").val(),
                $('#partner-service').find("option:selected").val()
            ).draw();
        },

        showSlideByIndex: function () {
            var activeImage = this.sliderItemsList[this.sliderActiveSlideIndex];
            if (this.sliderActiveSlideIndex == 0) {
                $(".ew-gallery-preview-wrap .prev").fadeOut();
            } else {
                $(".ew-gallery-preview-wrap .prev").fadeIn();
            }
            if (this.sliderActiveSlideIndex == this.sliderItemsList.length - 1) {
                $(".ew-gallery-preview-wrap .next").fadeOut();
            } else {
                $(".ew-gallery-preview-wrap .next").fadeIn();
            }

            $(".ew-gallery-preview-wrap .mask").css({"background-image": "url('" + activeImage + "')"}).fadeIn("narmal", function () {
                $(".ew-gallery-preview-wrap").css({"background-image": "url('" + activeImage + "')"});
                $(this).fadeOut();
            });
        },

        showGalleryPreview: function (el) {

            var localList = [],
                dataArgs = el.data(),
                group = dataArgs.group,
                activeIndex = dataArgs.index,
                relatedItemsSel = "a[data-group=" + group + "]",
                previewWrap = $("<div>").addClass("ew-gallery-preview-wrap").append($("<a href='#' class='close'>")).append($("<a href='#' class='prev'>")).append($("<a href='#' class='next'>")).append($("<div class='mask'>"));

            this.sliderActiveSlideIndex = activeIndex;

            $(relatedItemsSel).each(function () {

                localList.push($(this).attr("href"));

            });

            this.sliderItemsList = localList;

            $("body").addClass("openedGalleryView").append(previewWrap);

            this.showSlideByIndex();

            previewWrap.fadeIn();

        },

        hideGalleryPreview: function () {
            $(".ew-gallery-preview-wrap").fadeOut("normal", function () {
                $(this).remove();
                $("body").removeClass("openedGalleryView");
            });
        },

        showVideoPpup: function (el) {
            var videoUrl = el.attr("href"),
                popupWrap = $("<div>").addClass("ew-video-popup-wrap").append($("<a href='#' class='close'>")).append($("<div class='videoWrapper'>").append($("<iframe src='" + videoUrl + "' style='width:100%;border:0px none' allowfullscreen></iframe>")));
            console.log(videoUrl);

            $("body").addClass("openedGalleryView").append(popupWrap);
            $(".ew-video-popup-wrap").fadeIn();
        },

        hideVideoPopup: function () {
            $(".ew-video-popup-wrap").fadeOut("normal", function () {
                $(this).remove();
                $("body").removeClass("openedGalleryView");
            });
        },

        resetActiveMenuItemClass: function () {
            $('nav.main-menu .expanded .submenu').each(function () {
                $(this).fadeOut();
            })
            this.mainMenuFirstLevel.removeClass("expanded");//.addClass("active");
        },

        initEvents: function () {
            var self = this;

            $.fwbslider(self.fwContainer, {
                'delay': self.fwDelay,
                'fadeSpeed': self.fwFadeSpeed
            });

            if (jQuery().owlCarousel && self.newsCarousel.length > 0) {

                self.newsCarousel.owlCarousel({

                    itemsCustom: [
                        [0, 1],
                        [320, 1],
                        [650, 1],
                        [768, 1],
                        [992, 1],

                        [1200, 1]
                    ]
                });
            }

            $('body')
                .on('click', ".menu-logo .toggle-menu-button", function () {
                    if (!self.isMenuVisible()) {
                        self.showMenu()
                    } else {
                        self.hideMenu()
                    }
                    return false;
                })
                .on('click', ".main-menu ul li a", function () {
                    var closestUL = $(this).closest("ul"),
                        closestLI = $(this).closest("li"),
                        url = $(this).attr("href"),
                        isSmall = ($(wnd).width() < 768) ? true : false;

                    if (closestLI.hasClass("expanded") && closestLI.children(".submenu").length > 0 && !isSmall) {
                        self.resetActiveMenuItemClass();
                        return false;
                    }

                    //self.resetActiveMenuItemClass();

                    if (!closestLI.hasClass("expanded") && closestLI.children(".submenu").length > 0 && !isSmall) {
                        self.resetActiveMenuItemClass();
                        closestLI.addClass("expanded");
                        $('.expanded').find(".submenu").fadeIn();
                    }

                    if (closestLI.children(".submenu").length > 0 && isSmall) {
                        closestUL.children('li').each(function () {
                            $(this).removeClass("expanded");
                        });
                        closestLI.addClass("expanded");

                        $('.main-menu ul li > .submenu').each(function () {
                            if ($(this).parent().hasClass("expanded")) {
                                $(this).slideToggle();
                            } else {
                                $(this).slideUp();
                            }
                        });
                        return false;
                    }
                    window.location.href = url;

                    return false;
                    /*
                     var closestUL = $(this).closest("ul"),
                     closestLI = $(this).closest("li"),
                     url = $(this).attr("href"),
                     isSmall = ($(wnd).width() < 768) ? true : false;

                     closestUL.children('li').each(function () {
                     $(this).removeClass("expanded");
                     });
                     closestLI.addClass("expanded");
                     if (closestLI.children(".submenu").length > 0 && isSmall) {
                     closestUL.children('li').each(function () {
                     if (!$(this).hasClass("expanded")) {
                     $(this).slideToggle();
                     }
                     });
                     closestLI.children(".submenu").slideToggle();
                     return false;
                     }
                     window.location.href = url;
                     return true;*/
                })
                .on('click', ".news-list .control.next", function () {
                    self.newsCarousel.trigger('owl.next');
                    return false;
                })
                .on('click', ".news-list .control.prev", function () {
                    self.newsCarousel.trigger('owl.prev');
                    return false;
                })
                .on('click', "footer .meta a.search", function () {

                    if ($(wnd).width() > 800) {
                        self.showSearchButton.removeClass("up").fadeOut("fast", function () {
                            self.searchForm.fadeIn();
                        })
                    } else {
                        if (self.searchForm.hasClass("up")) {
                            self.searchForm.removeClass("up").fadeOut();
                        } else {
                            self.searchForm.addClass("up").fadeIn();
                        }
                    }
                    return false;
                })
                .on('click', ".image-preview", function () {
                    self.showGalleryPreview($(this));
                    return false;
                })
                .on('click', ".ew-gallery-preview-wrap .close", function () {
                    self.hideGalleryPreview();
                    return false;
                })
                .on('click', ".ew-gallery-preview-wrap .prev", function () {
                    --self.sliderActiveSlideIndex;
                    self.showSlideByIndex();
                    return false;
                })
                .on('click', ".ew-gallery-preview-wrap .next", function () {
                    ++self.sliderActiveSlideIndex;
                    self.showSlideByIndex();
                    return false;
                })
                .on('click', "a.video-popup", function () {
                    self.showVideoPpup($(this));
                    return false;
                })
                .on('click', ".ew-video-popup-wrap .close", function () {
                    self.hideVideoPopup();
                    return false;
                });

            self.setMinPageHeight();

            $(document).on('click', function (e) {
                if (self.menuContainer.is(':visible') && !self.menuContainer.is(e.target) && !self.menuContainer.has(e.target).length) {
                    if ($(".main-menu ul li > .submenu").is(':visible')) {
                        self.resetActiveMenuItemClass();
                    } else {
                        //self.hideMenu()
                    }
                }
            });

            if (jQuery().DataTable && $('#partners-table').length > 0) {

                self.partnersTable = $('#partners-table').DataTable({
                    paging: false,
                    language: {
                        processing: "Поиск",
                        loadingRecords: "Загрузка записей...",
                        zeroRecords: "Не найдено партнеров по таким критериям",
                        emptyTable: "Партнеров не найдено",
                        paginate: {
                            first: "Первый",
                            previous: "Предыдущий",
                            next: "Следующий",
                            last: "Последний"
                        }
                    }
                });

                self.partnersTable.columns().eq(0).each(function (colIdx) {
                    $('#partner-country', self.partnersTable.column(colIdx).footer()).on('change', function () {
                        self.partnersTable
                            .column(1).search($("#partner-country").val())
                            .column(2).search($("#partner-service").val())
                            .draw();
                    });
                    $('#partner-service', self.partnersTable.column(colIdx).footer()).on('change', function () {
                        self.partnersTable
                            .column(1).search($("#partner-country").val())
                            .column(2).search($("#partner-service").val())
                            .draw();
                    });
                });
            }

            if ($('#partner-country').length > 0) {
                $('#partner-country').change(function () {
                    self.filterGlobal();
                });
            }

            if ($('#partner-service').length > 0) {
                $('#partner-service').change(function () {
                    self.filterGlobal();
                });
            }

            self.setNewsPosition();

            $(wnd).resize(function () {
                self.setNewsPosition();

                if ($(wnd).width() > 768) {
                    self.searchForm.removeClass("up").fadeOut();
                }

                self.setMinPageHeight();

            });

            if (jQuery().styler) {

                $('select.styler').styler();
            }

            if ($('#map_canvas').length > 0 && jQuery().gmap) {

                $('#map_canvas').gmap().bind('init', function (ev, map) {
                    $('#map_canvas').gmap('addMarker', {'position': '40.1521651,47.615563,4', 'bounds': true}).click(function () {
                        $('#map_canvas').gmap('openInfoWindow', {'content': 'Hello World!'}, this);
                    });
                    $('#map_canvas').gmap('addMarker', {'position': '42.3207845,43.3713615,8', 'bounds': true}).click(function () {
                        $('#map_canvas').gmap('openInfoWindow', {'content': 'Hello World!'}, this);
                    });
                    $('#map_canvas').gmap('addMarker', {'position': '48.383022,31.1828699,6', 'bounds': true}).click(function () {
                        $('#map_canvas').gmap('openInfoWindow', {'content': 'Hello World!'}, this);
                    });
                    $('#map_canvas').gmap('addMarker', {'position': '48.005284,66.9045435,5', 'bounds': true}).click(function () {
                        $('#map_canvas').gmap('openInfoWindow', {'content': 'Hello World!'}, this);
                    });
                });
            }

        }
    };

    $(function () {
        window.EmbaWood = new EmbaWood();
    });

})(window, jQuery);